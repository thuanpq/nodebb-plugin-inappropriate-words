(function(module) {
    "use strict";

    var meta = module.parent.require('./meta');
    var controllers = require('./lib/controllers');
    var plugin = {settings: undefined};

    plugin.preinit = function (data, callback) {
        // Settings
        meta.settings.get('inappropriate-words', function (err, settings) {
            plugin.settings = settings;
            callback(null, data);
        });
    };

    plugin.init = function (params, callback) {
        var router = params.router,
            hostMiddleware = params.middleware,
            hostControllers = params.controllers;

        // We create two routes for every view. One API call, and the actual route itself.
        // Just add the buildHeader middleware to your route and NodeBB will take care of everything for you.

        router.get('/admin/plugins/inappropriate-words', hostMiddleware.admin.buildHeader, controllers.renderAdminPage);
        router.get('/api/admin/plugins/inappropriate-words', controllers.renderAdminPage);

        callback();
    };

    plugin.addAdminNavigation = function (header, callback) {
        header.plugins.push({
            route: '/plugins/inappropriate-words',
            icon: 'fa-tint',
            name: 'Inappropriate Words'
        });

        callback(null, header);
    };

    var embed = '******';

    plugin.parse = function (data, callback) {
        if (!data || !data.postData || !data.postData.content) {
            return callback(null, data);
        }

        if (plugin.settings !== undefined) {
            if (data.postData.content.indexOf("olala222") != -1) {
                //var arr_words = plugin.settings.inappropriatewords.split('\n');
                data.postData.content = data.postData.content.replace('olala222', plugin.settings);
            }
        } else {
            if (data.postData.content.indexOf("olala") != -1) {
                data.postData.content = data.postData.content.replace('olala', embed);
            }
        }

        callback(null, data);
    };

    module.exports = plugin;
}(module));