<div class="row">
	<div class="col-lg-9">
		<div class="panel panel-default">
			<div class="panel-heading">Inappropriate Words Settings</div>
			<div class="panel-body">
				<form role="form" class="inappropriate-words-settings">
					<p>
						Line by line for each inappropriate words
					</p>
					<div class="form-group">
                        <label for="inappropriatewords">Inappropriate Words</label>
						<input type="text" id="inappropriatewords" name="inappropriatewords" title="Inappropriate Words" class="form-control" placeholder="Inappropriate Words"><br />
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="col-lg-3">
		<div class="panel panel-default">
			<div class="panel-heading">Control Panel</div>
			<div class="panel-body">
				<button class="btn btn-primary" id="save">Save Settings</button>
			</div>
		</div>
	</div>
</div>
